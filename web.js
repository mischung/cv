/*jslint node : true */
"use strict";
//import React from 'react';
//import JsxParser form 'react-jsx-parser';

var express = require('express');
var app = express();
var doT = require('express-dot');
var db = require('./db.js').db;

app.configure(function () {
    app.use(express.bodyParser());
    app.use(app.router);
    app.use(express.errorHandler());
    app.use(express.methodOverride());
    app.use(express.static(__dirname + '/public'));
    app.set('view engine', 'dot');
    app.engine('html', doT.__express);
    app.set('views', __dirname + '/public/views');
});

app.get('/', function (request, response) {
    response.render('index.html', {layout : false, cache : false, name: 'martinChung', target: 'it'});
});
app.get('/CV-it', function (request, response) {
    response.render('index.html', {layout : false, cache : false, name: 'martinChung', target: 'it'});
});
app.get('/CV-biotech', function (request, response) {
    response.render('index.html', {layout : false, cache : false, name: 'martinChung', target: 'biotech'});
});

app.get('/CV/:entry', function (request, response) {
    console.log("Getting entry for: " + request.params.entry);
    response.send(db[request.params.entry]);
});
var port = process.env.PORT || 5000;
app.listen(port, function () {
    console.log('Listening to port' + port);
});
