"use strict";
var app = app || {};

app.Element = Backbone.RelationalModel.extend({
    urlRoot : "/CV/",
    initialize : function () {
        console.log("Initializing model" + this.get('name'));
    },
    relations : [{
        type : Backbone.HasMany,
        key : 'elements',
        relatedModel : 'app.Element',
        reverseRelation : {
            key : 'parent',
            includeInJSON : 'id'
        }
    }]
});

$(function () {
    app.cvData = new app.Element({id : 0});
});
