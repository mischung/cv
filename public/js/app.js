"use strict";
var app = app || {};

$(function () {
    app.cv = new app.TextView({
        model : app.cvData
    });
    app.cvData.fetch({
        reset : true,
        success : function () {
            console.log("Success fetching data");
            app.cv.render("page", app.cv.el.attributes.target.nodeValue);
        },
        error : function () {
            console.log("Error fetching data");
            this.trigger('change');
        }
    });   //get CV data
});
