/** @jsx React.DOM */
"use strict";
var app = app || {};

$(function () {
    var componentView = React.createClass({displayName: 'componentView',
        getInitialState : function () {
            return {
                visible : true       
            }
        }
        , render : function () {
            var comp;
            var name;
            console.log("Assembling view for node" + this.props.model);
            var compObj = this.props.get('viewComponent') || {};
            var componentName = compObj[viewStyle] || "none"; 
            var level = this.props.level || 1; //level of node
            var model = this.props.model;
            var viewStyle = this.props.viewStyle || "app";
            name = this.props.model.get('name') || "";
            comp = this.components[componentName] || this.components["none"];
            return comp( {model:model, viewStyle:viewStyle, level:level} )
        }
        , components : { textBox : React.createClass({ 
                render : function () {
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var childLevel = this.props.level + 1;
                        var name = child.get('name') || "";
                        var style = this.props.viewStyle;
                        return componentView( {model:child, viewStyle:style, level:childLevel} )
                    }, this);
                    return React.DOM.div(null, nodes);
                }
            })
            , navTabs : React.createClass({ 
                render : function () {
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var childLevel = this.props.level + 1;
                        var name = child.get('name') || "";
                        var classParam = "tab-pane";
                        var style = this.props.viewStyle;
                        if (key === 0) {
                            classParam = "tab-pane active";
                        }
                        return React.DOM.div( {className:classParam, id:name}, componentView( {model:child, viewStyle:style, level:childLevel} ) )
                    }, this);
                    var names = this.props.model.get('elements').map(function (child, key) {
                        var name = child.get('name') || "";
                        var ref = "#" + name;
                        return React.DOM.li(null, React.DOM.a( {href:ref, 'data-toggle':"tab"}, name));
                    }, this);
                    return (
                    React.DOM.div(null, 
                        React.DOM.ul( {className:"nav nav-tabs"}, 
                            names
                        ),
                        React.DOM.div( {className:"tab-content"}, 
                            nodes
                        )
                    )
                    );
                }
            })
            , timeList : React.createClass({ 
                render : function () {
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var childLevel = this.props.level + 1;
                        var name = child.get('name') || "";
                        var style = this.props.viewStyle;
                        return React.DOM.li( {className:"list-group-items"}, componentView( {model:child, viewStyle:style, level:childLevel} ))
                    }, this);
                    return React.DOM.ul( {className:"list-group"}, nodes);
                }
            })
            , list : React.createClass({ 
                render : function () {
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var childLevel = this.props.level + 1;
                        var name = child.get('name') || "";
                        var style = this.props.viewStyle;
                        return React.DOM.li( {className:"list-group-items"}, componentView( {model:child, viewStyle:style, level:childLevel} ))
                    }, this);
                    return React.DOM.ul( {className:"list-group"}, nodes)
                }
            })
            , pageHeader : React.createClass({ 
                render : function () {
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var level = this.props.level;
                        var childLevel = level + 1;
                        var name = child.get('name') || "";
                        var style = this.props.viewStyle;
                        var comp = React.DOM["h" + level];
                        return comp(null, name,React.DOM.p(null, componentView( {model:child, viewStyle:style, level:childLevel} )))
                    }, this);
                    return React.DOM.div( {className:"page-header"}, nodes);
                }
            })
            , none : React.createClass({ 
                render : function () {
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var level = this.props.level;
                        var childLevel = level + 1;
                        var name = child.get('name') || "";
                        var style = this.props.viewStyle;
                        var comp = React.DOM["h" + level];
                        return componentView( {model:child, viewStyle:style, level:childLevel} )
                    }, this);
                    return React.DOM.div(null, nodes);
                }
            })
        }
    });
    app.TextView = Backbone.View.extend({
        el : "#CV"
        , initialize : function () {
            console.log("Initializing model:" + this.model || "undefinded");
            this.listenTo(this.model, 'change', this.render);
        }
        , render : function () {
            console.log("Rendering element : " + $(this.el));
            React.renderComponent(componentView(
                {model:this.model}), this.$el.get(0));
            return this;
        }
    });




});