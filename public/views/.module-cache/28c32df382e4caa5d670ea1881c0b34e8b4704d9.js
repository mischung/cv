/** @jsx React.DOM */
"use strict";
var app = app || {};

$(function () {

    var showdown = new Showdown.converter();
    var getDateString = function (date) {
        //parse date from numbers e.g. 23-01-2012 to 23 Janurary, 2012, for 00-01-2012 the day value is ignored. Note: Does not check for correct day for that month. Assumes values are integers. If year is 0000 then the string "Now" is returned.
        var result = ""; //Output of string e.g. 10 January, 2019
        var splitDateNumbers; //array of date numbers in Day-Month-Year
        var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        var singleDigit, tenthDigit;
        if (date && _.isFunction(date.split)) {
                splitDateNumbers = date.split("-");
                if (Number(splitDateNumbers[0]) > 0) {
                    result = result + Number(splitDateNumbers[0]);
                    singleDigit = Number(splitDateNumbers[0].substr(splitDateNumbers[0].length - 1, 1));
                    tenthDigit = Number(splitDateNumbers[0].substr(splitDateNumbers[0].length - 2, 1)); 
                    if (singleDigit === 1 && tenthDigit !== 1) {
                        result = result + "st";
                    } else if (singleDigit === 2 && tenthDigit !== 1) {
                        result = result + "nd";
                    } else if (singleDigit === 2 && tenthDigit !== 1) {
                        result = result + "nd";
                    } else if (singleDigit === 3 && tenthDigit !== 1) {
                        result = result + "rd";
                    } else {
                        result = result + "th";
                    };
                };
                
                if (Number(splitDateNumbers[1]) > 0) {
                    result = result + " " + months[Number(splitDateNumbers[1] - 1)] + ", ";
                };
                if (Number(splitDateNumbers[2]) > 0) {
                    result = result + Number(splitDateNumbers[2]);
                } else if (Number(splitDateNumbers[2]) === 0) {
                    result = "present";
                };
        }
        return result;    
    }
    var componentView = React.createClass({displayName: 'componentView',
        getInitialState : function () {
            return {
                visible : true       
            }
        }
        , render : function () {
            var comp;
            var viewStyle = this.props.viewStyle || "app";
            var compObj = this.props.model.get('viewComponent') || {};
            var componentName = compObj[viewStyle] || "none"; 
            var level = this.props.level || 1; //level of node
            var model = this.props.model;
            comp = this.components[componentName] || this.components["none"];
            return comp( {model:model, viewStyle:viewStyle, level:level} )
        }
        , components : { textBox : React.createClass({ 
                render : function () {
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var level = this.props.level;
                        var childLevel = level + 1;
                        var name = child.get('name') || "";
                        var style = this.props.viewStyle;
                        var headerTag = React.DOM["h" + level]; 
                        return React.DOM.div(null, headerTag(null, React.DOM.span( {dangerouslySetInnerHTML:{__html : showdown.makeHtml(name.toString())}} ),componentView( {model:child, viewStyle:style, level:childLevel} )))
                    }, this);
                    return React.DOM.div(null, nodes);
                }
            })
            , panel : React.createClass({
            	render : function () {
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var level = this.props.level;
                        var childLevel = level + 1;
                        var name = child.get('name') || "";
                        var panelHeader = (React.DOM.div(null ));
                        var panelBody = (React.DOM.div(null ));
                        var style = this.props.viewStyle;
                        var headerTag = React.DOM["h" + level]; 
                        if (name !== "") {
                        	panelHeader = (React.DOM.div( {className:"panel-heading"}, headerTag(null, React.DOM.span( {dangerouslySetInnerHTML:{__html : showdown.makeHtml(name.toString())}} ))));
                        }
                        if (child.get('elements') && child.get('elements').length > 0 ) {
                        	panelBody = (React.DOM.div( {className:"panel-body"}, componentView( {model:child, viewStyle:style, level:childLevel} )));
                        }
                        return React.DOM.div( {className:"panel panel-default"}, panelHeader,panelBody)
                    }, this);
                    return React.DOM.div(null, nodes)
            	}
            })
            , navTabs : React.createClass({ 
                render : function () {
                    console.log("Rendering navTabs")
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var childLevel = this.props.level + 1;
                        var idName = child.cid;
                        var classParam = "tab-pane fade";
                        var style = this.props.viewStyle;
                        if (key === 0) {
                            classParam = "tab-pane fade in active";
                        }
                        return React.DOM.div( {className:classParam, id:idName}, componentView( {model:child, viewStyle:style, level:childLevel} ) )
                    }, this);
                    var names = this.props.model.get('elements').map(function (child, key) {
                        var name = child.get('name') || "";
                        var classParam = "";
                        var ref = "#" + child.cid;
                        if (key === 0) {
                            classParam = "active";
                        }

                        return (React.DOM.li( {className:classParam}, React.DOM.a( {href:ref, 'data-toggle':"tab", dangerouslySetInnerHTML:{__html : showdown.makeHtml(name.toString())}} )));
                    }, this);
                    console.log("Got names: " + names);
                    return (
                    React.DOM.div(null, 
                        React.DOM.ul( {className:"nav nav-tabs"}, 
                            names
                        ),
                        React.DOM.div( {className:"tab-content"}, 
                            nodes
                        )
                    )
                    );
                }
            })
            , timeList : React.createClass({ 
                render : function () {
                    console.log("Rendering timeList")
                    var nodes = this.props.model.get('elements').map(function (child, key) {                    
                        var childLevel = this.props.level + 1;
                        var name = child.get('name') || "";
                        var style = this.props.viewStyle;
                        var dateFrom = getDateString(child.get('timeStart')) || "";
                        var dateTo = getDateString(child.get('timeEnd')) || "";
                        var dateString;
                        if (dateFrom.length > 0) {
                            dateString = dateFrom + " to " + dateTo;
                        } else {
                            dateString = dateTo;
                        };
                        return (React.DOM.div(null, React.DOM.dt(null, dateString),React.DOM.dd(null, React.DOM.span( {dangerouslySetInnerHTML:{__html : showdown.makeHtml(name.toString())}} ),componentView( {model:child, viewStyle:style, level:childLevel} ))));
                    }, this);
                    return (React.DOM.dl( {className:"dl-horizontal"}, nodes));
                }
            })
            , list : React.createClass({ 
                render : function () {
                    console.log("Rendering list")
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var childLevel = this.props.level + 1;
                        var name = child.get('name') || "";
                        var style = this.props.viewStyle;
                        return (React.DOM.li(null, React.DOM.span( {className:"list-group-items", dangerouslySetInnerHTML:{__html : showdown.makeHtml(name.toString())}} ),componentView( {model:child, viewStyle:style, level:childLevel} )));
                    }, this);
                    return (React.DOM.div(null, React.DOM.ul( {className:"list-group"}, nodes)));
                }
            })
            , inlineList : React.createClass({
            	handleClick : function (e) {
            		var func = $(this.refs.payload.getDOMNode()).triggerFunc;
            		if (typeof func === 'function') {
            			func({ html : true, content : this.refs.payload.content});
            		}
            	} 
                , render : function () {
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var childLevel = this.props.level + 1;
                        var name = child.get('name') || "";
                        var style = this.props.viewStyle;
                        return (React.DOM.li(null, 
                        			React.DOM.button( {id:child.cid, type:"button", 
                        				onClick:this.handleClick, 
                        				className:"btn btn-primary", 
                        				dangerouslySetInnerHTML:{__html : showdown.makeHtml(name.toString())}}),
                        			componentView( {ref:"payload", model:child, viewStyle:style, level:childLevel} )
                        		));
                    }, this);
                    return (React.DOM.ul( {className:"list-inline"}, nodes)	);
                }
            })
            , pageHeader : React.createClass({ 
                render : function () {
                    console.log("Rendering pageHeader")
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var level = this.props.level;
                        var childLevel = level + 1;
                        var name = child.get('name') || "";
                        var style = this.props.viewStyle;
                        var comp = React.DOM["h" + level];
                        return (React.DOM.div( {className:"page-header"}, comp( {dangerouslySetInnerHTML:{__html : showdown.makeHtml(name.toString())}} ),React.DOM.p(null, componentView( {model:child, viewStyle:style, level:childLevel} ))));
                    }, this);
                    return (React.DOM.div(null, nodes));
                }
            })
            , bubble : React.createClass({
            	componentDidMount : function () {
                	//this.triggerFunc = $(this.getDOMNode()).tooltip();
            	} 
                , render : function () {
                    console.log("Rendering none")
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var level = this.props.level;
                        var childLevel = level + 1;
                        var name = child.get('name') || "";
                        var style = this.props.viewStyle;
                        var comp = React.DOM["h" + level];
                        return (React.DOM.div( {className:"tooltip-inner", dangerouslySetInnerHTML:{__html : showdown.makeHtml(name.toString())}} ));
                    }, this);
                     return React.DOM.div( {classname:"tooltip"}, nodes);
                }
            })
            , none : React.createClass({ 
                render : function () {
                    console.log("Rendering none")
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var level = this.props.level;
                        var childLevel = level + 1;
                        var name = child.get('name') || "";
                        var style = this.props.viewStyle;
                        var comp = React.DOM["h" + level];
                        return (React.DOM.div(null, comp( {dangerouslySetInnerHTML:{__html : showdown.makeHtml(name.toString())}} ),componentView( {model:child, viewStyle:style, level:childLevel} )));
                    }, this);
                    return React.DOM.div(null, nodes);
                }
            })
        }
    });
    app.TextView = Backbone.View.extend({
        el : "#CV"
        , initialize : function () {
            console.log("Initializing model:" + this.model || "undefinded");
            this.listenTo(this.model, 'change', this.render);
        }
        , render : function (viewStyle) {
            viewStyle = viewStyle || "";
            console.log("Rendering element : " + $(this.el));
            React.renderComponent(componentView(
                {model:this.model, viewStyle:viewStyle} ), this.$el.get(0));
            return this;
        }
    });




});