/** @jsx React.DOM */
"use strict";
var app = app || {};

$(function () {
    var componentView = React.createClass({displayName: 'componentView',
        getInitialState : function () {
            return {
                visible : true       
            }
        }
        , render : function () {
            var comp;
            var name;
            var compObj = this.props.model.get('viewComponent') || {};
            var componentName = compObj[viewStyle] || "none"; 
            var level = this.props.level || 1; //level of node
            var model = this.props.model;
            var viewStyle = this.props.viewStyle || "app";
            name = this.props.model.get('name') || "";
            comp = this.components[componentName] || this.components["none"];
            return React.DOM.div(null, comp( {model:model, viewStyle:viewStyle, level:level} ))
        }
        , components : { textBox : React.createClass({ 
                render : function () {
                    console.log("Rendering textBox")
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var childLevel = this.props.level + 1;
                        var name = child.get('name') || "";
                        var style = this.props.viewStyle;
                        return componentView( {model:child, viewStyle:style, level:childLevel} )
                    }, this);
                    var names = this.props.model.get('name') 
                    return React.DOM.div(null, React.DOM.h1(null, names),nodes);
                }
            })
            , navTabs : React.createClass({ 
                render : function () {
                    console.log("Rendering navTabs")
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var childLevel = this.props.level + 1;
                        var name = child.get('name') || "";
                        var classParam = "tab-pane";
                        var style = this.props.viewStyle;
                        if (key === 0) {
                            classParam = "tab-pane active";
                        }
                        return React.DOM.div( {class:classParam, id:name}, componentView( {model:child, viewStyle:style, level:childLevel} ) )
                    }, this);
                    var names = this.props.model.get('elements').map(function (child, key) {
                        var name = child.get('name') || "";
                        var ref = "#" + name;
                        return React.DOM.li(null, React.DOM.a( {href:ref, 'data-toggle':"tab"}, name));
                    }, this);
                    console.log("Got names: " + names);
                    return (
                    React.DOM.div(null, 
                        React.DOM.ul( {class:"nav nav-tabs"}, 
                            names
                        ),
                        React.DOM.div( {class:"tab-content"}, 
                            nodes
                        )
                    )
                    );
                }
            })
            , timeList : React.createClass({ 
                render : function () {
                    console.log("Rendering timeList")
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var childLevel = this.props.level + 1;
                        var name = child.get('name') || "";
                        var style = this.props.viewStyle;
                        return React.DOM.li( {class:"list-group-items"}, componentView( {model:child, viewStyle:style, level:childLevel} ))
                    }, this);
                    return React.DOM.ul( {class:"list-group"}, nodes);
                }
            })
            , list : React.createClass({ 
                render : function () {
                    console.log("Rendering list")
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var childLevel = this.props.level + 1;
                        var name = child.get('name') || "";
                        var style = this.props.viewStyle;
                        return React.DOM.li( {class:"list-group-items"}, componentView( {model:child, viewStyle:style, level:childLevel} ))
                    }, this);
                    return React.DOM.ul( {class:"list-group"}, nodes)
                }
            })
            , pageHeader : React.createClass({ 
                render : function () {
                    console.log("Rendering pageHeader")
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var level = this.props.level;
                        var childLevel = level + 1;
                        var name = child.get('name') || "";
                        var style = this.props.viewStyle;
                        var comp = React.DOM["h" + level];
                        return comp(null, name,React.DOM.p(null, componentView( {model:child, viewStyle:style, level:childLevel} )))
                    }, this);
                    return React.DOM.div( {class:"page-header"}, nodes);
                }
            })
            , none : React.createClass({ 
                render : function () {
                    console.log("Rendering none")
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var level = this.props.level;
                        var childLevel = level + 1;
                        var name = child.get('name') || "";
                        var style = this.props.viewStyle;
                        var comp = React.DOM["h" + level];
                        return componentView( {model:child, viewStyle:style, level:childLevel} )
                    }, this);
                    return React.DOM.div(null, nodes);
                }
            })
        }
    });
    app.TextView = Backbone.View.extend({
        el : "#CV"
        , initialize : function () {
            console.log("Initializing model:" + this.model || "undefinded");
            this.listenTo(this.model, 'change', this.render);
        }
        , render : function () {
            console.log("Rendering element : " + $(this.el));
            React.renderComponent(componentView(
                {model:this.model}), this.$el.get(0));
            return this;
        }
    });




});