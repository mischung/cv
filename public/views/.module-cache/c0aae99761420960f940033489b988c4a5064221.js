/** @jsx React.DOM */
"use strict";
var app = app || {};

$(function () {
    var Components = {
        "text-box" : React.createClass({
            render : function () {
                return (
                    React.DOM.a(null, 
                        this.props.text
                    )                
                );
            }
        })
        , "nav-tabs" : React.createClass({
            render : function () {
                return (
                    React.DOM.ul( {class:"nav nav-tabs"}, 
                        this.props.text
                    )
                );
            }
        })
        , "time-list" : React.createClass({
            render : function () {
                return (
                    React.DOM.ul( {class:"nav nav-tabs"}, 
                        this.props.text
                    )
                );
            }
        })
        , "list" : React.createClass({
            render : function () {
                return (
                    React.DOM.ul( {class:"nav nav-tabs"}, 
                        this.props.text
                    )
                );
            }
        })
        , "page-header" : React.createClass({
            render : function () {
                return (
                    React.DOM.div( {class:"page-header"}, 
                        this.props.text
                    )
                );
            }
        })

    };
    app.TextView = Backbone.View.extend({
        el : "#CV"
        , initialize : function () {
            console.log("Initializing model:" + this.model || "undefinded");
            this.listenTo(this.model, 'change', this.render);
        }
        , render : function () {
            console.log("Rendering element : " + $(this.el));
            var component = Components.text-box;
            React.renderComponent(component(
                {text:JSON.stringify(this.model)}), this.$el.get(0));
            return this;
        }
    });




});