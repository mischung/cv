/** @jsx React.DOM */
"use strict";
var app = app || {};

$(function () {
    var componentView = React.createClass({displayName: 'componentView',
        getInitialState : function () {
            return {
                visible : true       
            }
        }
        , render : function () {
            var componentName;
            var comp;
            var name;
            var level = this.props.level || 1; //level of node
            var model = this.props.model;
            var viewStyle = this.props.viewStyle || "app";
            name = this.props.model.name || "";
            componentName = this.props.model.viewComponent[viewStyle] || "none"; 
            comp = this.components[componentName] || this.components["none"];
            return comp( {model:model, viewStyle:viewStyle, level:level} )
        }
        , components : { textBox : React.createClass({ 
                render : function () {
                    var nodes = _.map(this.props.model.elements || [], function (child, key) {
                        var childLevel = this.props.level + 1;
                        var name = child.name || "";
                        var style = this.props.viewStyle;
                        return componentView( {model:child, viewStyle:style, level:childLevel} )
                    });
                    return {nodes}
                }
            })
            , navTabs : React.createClass({ 
                render : function () {
                    var nodes = _.map(this.props.model.elements || [], function (child, key) {
                        var childLevel = this.props.level + 1;
                        var name = child.name || "";
                        var classParam = "tab-pane";
                        var style = this.props.viewStyle;
                        if (key === 0) {
                            classParam = "tab-pane active";
                        }
                        return React.DOM.div( {class:classParam, id:name}, componentView( {model:child, viewStyle:style, level:childLevel} ) )
                    });
                    var names = _.map(model.elements || [], function (child, key) {
                        var name = child.name || "";
                        var ref = "#" + name;
                        return il(null, React.DOM.a( {href:ref, 'data-toggle':"tab"}, name));
                    });
                    return (
                    React.DOM.div(null, 
                        React.DOM.ul( {class:"nav nav-tabs"}, 
                            names
                        ),
                        React.DOM.div( {class:"tab-content"}, 
                            nodes
                        )
                    )
                    );
                }
            })
            , timeList : React.createClass({ 
                render : function () {
                    var nodes = _.map(this.props.model.elements || [], function (child, key) {
                        var childLevel = this.props.level + 1;
                        var name = child.name || "";
                        var style = this.props.viewStyle;
                        return React.DOM.li( {class:"list-group-items"}, componentView( {model:child, viewStyle:style, level:childLevel} ))
                    });
                    return React.DOM.ul( {class:"list-group"}, nodes)
                }
            })
            , list : React.createClass({ 
                render : function () {
                    var nodes = _.map(this.props.model.elements || [], function (child, key) {
                        var childLevel = this.props.level + 1;
                        var name = child.name || "";
                        var style = this.props.viewStyle;
                        return React.DOM.li( {class:"list-group-items"}, componentView( {model:child, viewStyle:style, level:childLevel} ))
                    });
                    return React.DOM.ul( {class:"list-group"}, nodes)
                }
            })
            , pageHeader : React.createClass({ 
                render : function () {
                    var nodes = _.map(this.props.model.elements || [], function (child, key) {
                        var level = this.props.level;
                        var childLevel = level + 1;
                        var name = child.name || "";
                        var style = this.props.viewStyle;
                        var comp = React.DOM["h" + level];
                        return comp(null, name,React.DOM.p(null, componentView( {model:child, viewStyle:style, level:childLevel} )))
                    });
                    return React.DOM.div( {class:"page-header"}, nodes);
                }
            })
            , none : React.createClass({ 
                render : function () {
                    var nodes = _.map(this.props.model.elements || [], function (child, key) {
                        var level = this.props.level;
                        var childLevel = level + 1;
                        var name = child.name || "";
                        var style = this.props.viewStyle;
                        var comp = React.DOM["h" + level];
                        return componentView( {model:child, viewStyle:style, level:childLevel} )
                    });
                    return React.DOM.div(null, nodes);
                }
            })
        }
    });
    app.TextView = Backbone.View.extend({
        el : "#CV"
        , initialize : function () {
            console.log("Initializing model:" + this.model || "undefinded");
            this.listenTo(this.model, 'change', this.render);
        }
        , render : function () {
            console.log("Rendering element : " + $(this.el));
            var component = Components["text-box"];
            React.renderComponent(component(
                {model:this.model}), this.$el.get(0));
            return this;
        }
    });




});