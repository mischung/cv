/** @jsx React.DOM */
//"use strict";
var app = app || {};

$(function () {
	
    var TextViewComponent = React.createClass({displayName: 'TextViewComponent',
        render : function () {
            return (
                React.DOM.a(null, 
                    this.props.text
                )                
            );
        }
    });
    
    
    
    app.TextView = Backbone.View.extend({
        el : "#CV"
        , initialize : function () {
            console.log("Initializing model:" + this.model || "undefinded");
            this.listenTo(this.model, 'change', this.render);
        }
        , render : function () {
            console.log("Rendering element : " + $(this.el));
            React.renderComponent(TextViewComponent(
                {text:JSON.stringify(this.model)}), this.$el.get(0));
            return this;
        }
    });




});