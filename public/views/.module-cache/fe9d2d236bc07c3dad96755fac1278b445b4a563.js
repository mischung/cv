/** @jsx React.DOM */
"use strict";
var app = app || {};

$(function () {
    var getDateString = function (date) {
        //parse date from numbers e.g. 23-01-2012 to 23 Janurary, 2012, for 00-01-2012 the day value is ignored. Note: Does not check for correct day for that month. Assumes values are integers. If year is 0000 then the string "Now" is returned.
        var result = ""; //Output of string e.g. 10 January, 2019
        var splitDateNumbers; //array of date numbers in Day-Month-Year
        var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var singleDigit, tenthDigit;
        if (date && _.isFunction(date.split)) {
                splitDateNumbers = date.split("-");
                if (Number(splitDateNumbers[0]) > 0) {
                    result = result + Number(splitDateNumbers[0]);
                    singleDigit = Number(splitDateNumbers[0].substr(splitDateNumbers[0].length - 1, 1));
                    tenthDigit = Number(splitDateNumbers[0].substr(splitDateNumbers[0].length - 2, 1)); 
                    if (singleDigit === 1 && tenthDigit !== 1) {
                        result = result + "st";
                    } else if (singleDigit === 2 && tenthDigit !== 1) {
                        result = result + "nd";
                    } else if (singleDigit === 2 && tenthDigit !== 1) {
                        result = result + "nd";
                    } else if (singleDigit === 3 && tenthDigit !== 1) {
                        result = result + "rd";
                    } else {
                        result = result + "th";
                    };
                };
                
                if (Number(splitDateNumbers[1]) > 0) {
                    result = result + " " + months[Number(splitDateNumbers[1] - 1)] + ", ";
                };
                if (Number(splitDateNumbers[2]) > 0) {
                    result = result + Number(splitDateNumbers[2]);
                } else if (Number(splitDateNumbers[2]) === 0) {
                    result = "present";
                };
        }
        return result;    
    }
    var componentView = React.createClass({displayName: 'componentView',
        getInitialState : function () {
            return {
                visible : true       
            }
        }
        , render : function () {
            var comp;
            var name;
            var viewStyle = this.props.viewStyle || "app";
            var compObj = this.props.model.get('viewComponent') || {};
            var componentName = compObj[viewStyle] || "none"; 
            var level = this.props.level || 1; //level of node
            var model = this.props.model;
            name = this.props.model.get('name') || "";
            comp = this.components[componentName] || this.components["none"];
            return comp( {model:model, viewStyle:viewStyle, level:level} )
        }
        , components : { textBox : React.createClass({ 
                render : function () {
                    console.log("Rendering textBox")
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var level = this.props.level;
                        var childLevel = level + 1;
                        var name = child.get('name') || "";
                        var style = this.props.viewStyle;
                        var headerTag = React.DOM["h" + level]; 
                        return React.DOM.div(null, headerTag(null, name),componentView( {model:child, viewStyle:style, level:childLevel} ))
                    }, this);
                    var names = this.props.model.get('name') 
                    return React.DOM.div(null, nodes);
                }
            })
            , navTabs : React.createClass({ 
                render : function () {
                    console.log("Rendering navTabs")
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var childLevel = this.props.level + 1;
                        var name = child.get('name') || "";
                        var classParam = "tab-pane";
                        var style = this.props.viewStyle;
                        if (key === 0) {
                            classParam = "tab-pane active";
                        }
                        return React.DOM.div( {className:classParam, id:name}, componentView( {model:child, viewStyle:style, level:childLevel} ) )
                    }, this);
                    var names = this.props.model.get('elements').map(function (child, key) {
                        var name = child.get('name') || "";
                        var ref = "#" + name;
                        return (React.DOM.li(null, React.DOM.a( {href:ref, 'data-toggle':"tab"}, name)));
                    }, this);
                    console.log("Got names: " + names);
                    return (
                    React.DOM.div(null, 
                        React.DOM.ul( {className:"nav nav-tabs"}, 
                            names
                        ),
                        React.DOM.div( {className:"tab-content"}, 
                            nodes
                        )
                    )
                    );
                }
            })
            , timeList : React.createClass({ 
                render : function () {
                    console.log("Rendering timeList")
                    var nodes = this.props.model.get('elements').map(function (child, key) {                    
                        var childLevel = this.props.level + 1;
                        var name = child.get('name') || "";
                        var style = this.props.viewStyle;
                        var dateFrom = getDateString(child.get('timeStart')) || "";
                        var dateTo = getDateString(child.get('timeEnd')) || "";
                        var dateString;
                        if (dateFrom.length > 0) {
                            dateString = dateFrom + " to " + dateTo;
                        } else {
                            dateString = dateTo;
                        };
                        return (React.DOM.div(null, React.DOM.dt(null, dateString),React.DOM.dd(null, name,componentView( {model:child, viewStyle:style, level:childLevel} ))));
                    }, this);
                    return (React.DOM.dl( {className:"list-group"}, nodes));
                }
            })
            , list : React.createClass({ 
                render : function () {
                    console.log("Rendering list")
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var childLevel = this.props.level + 1;
                        var name = child.get('name') || "";
                        var style = this.props.viewStyle;
                        return (React.DOM.li( {className:"list-group-items"}, name,componentView( {model:child, viewStyle:style, level:childLevel} )));
                    }, this);
                    return (React.DOM.ul( {className:"list-group"}, nodes));
                }
            })
            , pageHeader : React.createClass({ 
                render : function () {
                    console.log("Rendering pageHeader")
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var level = this.props.level;
                        var childLevel = level + 1;
                        var name = child.get('name') || "";
                        var style = this.props.viewStyle;
                        var comp = React.DOM["h" + level];
                        return (comp(null, name,React.DOM.p(null, componentView( {model:child, viewStyle:style, level:childLevel} ))));
                    }, this);
                    return (React.DOM.div( {className:"page-header"}, nodes));
                }
            })
            , none : React.createClass({ 
                render : function () {
                    console.log("Rendering none")
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var level = this.props.level;
                        var childLevel = level + 1;
                        var name = child.get('name') || "";
                        var style = this.props.viewStyle;
                        var comp = React.DOM["h" + level];
                        return (React.DOM.div(null, React.DOM.p(null, name),componentView( {model:child, viewStyle:style, level:childLevel} )));
                    }, this);
                    return React.DOM.div(null, nodes);
                }
            })
        }
    });
    app.TextView = Backbone.View.extend({
        el : "#CV"
        , initialize : function () {
            console.log("Initializing model:" + this.model || "undefinded");
            this.listenTo(this.model, 'change', this.render);
        }
        , render : function () {
            console.log("Rendering element : " + $(this.el));
            React.renderComponent(componentView(
                {model:this.model, viewStyle:"app"} ), this.$el.get(0));
            return this;
        }
    });




});