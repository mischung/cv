"use strict";
var app = app || {};
$(function () {

    var getDateString = function (date) {
        //parse date from numbers e.g. 23-01-2012 to 23 Janurary, 2012, for 00-01-2012 the day value is ignored. Note: Does not check for correct day for that month. Assumes values are integers. If year is 0000 then the string "Now" is returned.
        var result = ""; //Output of string e.g. 10 January, 2019
        var splitDateNumbers; //array of date numbers in Day-Month-Year
        var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        var singleDigit, tenthDigit;
        if (date && _.isFunction(date.split)) {
                splitDateNumbers = date.split("-");
                if (Number(splitDateNumbers[0]) > 0) {
                    result = result + Number(splitDateNumbers[0]);
                    singleDigit = Number(splitDateNumbers[0].substr(splitDateNumbers[0].length - 1, 1));
                    tenthDigit = Number(splitDateNumbers[0].substr(splitDateNumbers[0].length - 2, 1));
                    if (singleDigit === 1 && tenthDigit !== 1) {
                        result = result + "st";
                    } else if (singleDigit === 2 && tenthDigit !== 1) {
                        result = result + "nd";
                    } else if (singleDigit === 2 && tenthDigit !== 1) {
                        result = result + "nd";
                    } else if (singleDigit === 3 && tenthDigit !== 1) {
                        result = result + "rd";
                    } else {
                        result = result + "th";
                    };
                };
                if (Number(splitDateNumbers[1]) > 0) {
                    result = result + " " + months[Number(splitDateNumbers[1] - 1)] + ", ";
                };
                if (Number(splitDateNumbers[2]) > 0) {
                    result = result + Number(splitDateNumbers[2]);
                } else if (Number(splitDateNumbers[2]) === 0) {
                    result = "present";
                };
        }
        return result;
    };
    var getVisibility = function(child, target) {
        var result = true;
        var targetKey = target || "all";
        var comp = child.get('viewComponent') || {};
        var targetVisibility = comp[targetKey] || "visible";
        if (targetVisibility === "hidden") {
            result = false;
        } else if (targetVisibility === "visible") {
            result = true;
        }
        return result;
    };
    var ComponentView = React.createClass({
        getInitialState : function () {
            var target = this.props.target || "all";
            return {
                visible : true,
                target: target
            };
        }
        , render : function () {
            var viewStyle = this.props.viewStyle || "app";
            var compObj = this.props.model.get('viewComponent') || {};
            var componentName = compObj[viewStyle] || "none";
            var level = this.props.level || 1; //level of node
            var model = this.props.model;
            var Comp = this.components[componentName] || this.components["none"];
            var target = this.props.target || "all";
            return <Comp model={model} viewStyle={viewStyle} level={level} target={target} />;
        }
        , components : { textBox : React.createClass({
                render : function () {
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var level = this.props.level;
                        var childLevel = level + 1;
                        var name = child.get('name') || "";
                        var style = this.props.viewStyle;
                        var HeaderTag = "h" + level;
                        if (getVisibility(child, this.props.target)) {
                          return <div><HeaderTag><span dangerouslySetInnerHTML={ {__html: marked(name.toString())}} />
				                                <ComponentView model={child} viewStyle={style} level={childLevel} target={this.props.target}/></HeaderTag></div>;
                        } else {
                          return null;
                        }
                    }, this);
                    return <div>{nodes}</div>;
                }
            })
            , panel : React.createClass({
            	render : function () {
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var level = this.props.level;
                        var childLevel = level + 1;
                        var name = child.get('name') || "";
                        var panelHeader = (<div />);
                        var panelBody = (<div />);
                        var style = this.props.viewStyle;
                        var HeaderTag = "h" + level;
                        if (name !== "") {
                        	panelHeader = (<div className="panel-heading"><HeaderTag><span dangerouslySetInnerHTML={ {__html: marked(name.toString())}} /></HeaderTag></div>);
                        }
                        if (child.get('elements') && child.get('elements').length > 0 ) {
                        	  panelBody = (<div className="panel-body"><ComponentView model={child} viewStyle={style} level={childLevel} target={this.props.target}  /></div>);
                        }
                        if (getVisibility(child, this.props.target)) {
                          return <div className="panel panel-default">{panelHeader}{panelBody}</div>;
                        } else {
                          return null;
                        }
                    }, this);
                  return <div>{nodes}</div>;
            	}
            })
            , navTabs : React.createClass({
                render : function () {
                    console.log("Rendering navTabs");
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var childLevel = this.props.level + 1;
                        var idName = child.cid;
                        var classParam = "tab-pane fade";
                        var style = this.props.viewStyle;
                        if (key === 0) {
                            classParam = "tab-pane fade in active";
                        }
                        if (getVisibility(child, this.props.target)) {
                            return <div className={classParam} id={idName}><ComponentView model={child} viewStyle={style} level={childLevel} target={this.props.target}/></div>;
                        } else {
                          return null;
                        }
                    }, this);
                    var names = this.props.model.get('elements').map(function (child, key) {
                        var name = child.get('name') || "";
                        var classParam = "";
                        var ref = "#" + child.cid;
                        if (key === 0) {
                            classParam = "active";
                        }
                        if (getVisibility(child, this.props.target)) {
                          return (<li className={classParam}><a href={ref} data-toggle="tab" dangerouslySetInnerHTML={ {__html: marked(name.toString())}} /></li>);
                        } else {
                          return null;
                        }
                    }, this);
                    console.log("Got names: " + names);
                    return (
                    <div>
                        <ul className="nav nav-tabs">
                            {names}
                        </ul>
                        <div className="tab-content">
                            {nodes}
                        </div>
                    </div>
                    );
                }
            })
            , timeList : React.createClass({
                render : function () {
                    console.log("Rendering timeList");
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var childLevel = this.props.level + 1;
                        var name = child.get('name') || "";
                        var style = this.props.viewStyle;
                        var dateFrom = getDateString(child.get('timeStart')) || "";
                        var dateTo = getDateString(child.get('timeEnd')) || "";
                        var dateString;
                        if (dateFrom.length > 0) {
                            dateString = dateFrom + " to " + dateTo;
                        } else {
                            dateString = dateTo;
                        };
                        if (getVisibility(child, this.props.target)) {
                        return (<div className="row">
					<div className="time-column">{dateString}</div>
          <div className="event-column">
						<span dangerouslySetInnerHTML={ {__html: marked(name.toString())}} />
						<ComponentView model={child} viewStyle={style} level={childLevel} target={this.props.target}/>
					</div>
				                        </div>);
                        } else {
                            return null;
                        }
                    }, this);
                    return (<div className="dl-horizontal">{nodes}</div>);
                }
            })
            , list : React.createClass({
                render : function () {
                    console.log("Rendering list");
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var childLevel = this.props.level + 1;
                        var name = child.get('name') || "";
                        var style = this.props.viewStyle;
                        if (getVisibility(child, this.props.target)) {
                        return (<li><div className="list-group-items" dangerouslySetInnerHTML={ {__html: marked(name.toString())}} />
				                          <ComponentView model={child} viewStyle={style} level={childLevel} target={this.props.target}/></li>);
                        } else {
                            return null;
                        }
                    }, this);
                    return (<div><ul className="list-group">{nodes}</ul></div>);
                }
            })
            , inlineList : React.createClass({
                render : function () {
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var childLevel = this.props.level + 1;
                        var name = child.get('name') || "";
						var content;
                        content = child.get('elements').map(function (el) {
						                    return marked(el.get('name'));
                            }).join(",") || "";
			//			React.renderToString(<ComponentView model={child} level={childLevel} className="invisible-on-print"/>
			//				, function (html) {
			//				content = html
			//				console.log("Rendering the HTML: " + html);
		        //   	 	}, this);
                        return (<li className="btn-group">
                        			<button id={child.cid} type="button"
                        				data-container="body"
                        				data-placement="bottom"
                        				className="btn btn-info haspopover"
                        				data-toggle="popover"
                        				data-content={content}
                        				dangerouslySetInnerHTML={ {__html: marked(name.toString())}} />
                        		</li>);
                    }, this);
                    return (<ul className="list-inline">{nodes}</ul>	);
                }
            })
            , titleHeader : React.createClass({
                 render : function () {
                     console.log("Rendering titleHeader");
                    return (
			                    <div className="row title-header">
	                    		{this.props.model.get('elements').map(function (child, key) {
			                        var level = this.props.level;
			                        var childLevel = level + 1;
			                        var name = child.get('name') || "";
			                        var style = this.props.viewStyle;
				                var classNames = "column";
					        var Comp = "h" + level;
			                        if ((key + 1) % 2 > 0)
			                        {
			                        	classNames = classNames + "-left";
			                        } else {
			                        	classNames = classNames + "-right";
			                        }
                              if (getVisibility(child, this.props.target)) {
	        		                return (<div className={classNames}>
							<Comp  dangerouslySetInnerHTML={ {__html: marked(name.toString())}} />
							                          <p><ComponentView model={child} viewStyle={style} level={childLevel} target={this.props.target}/></p>
							                        </div>);
                              } else {
                                  return null;
                              }
		   			    }, this)}
			</div>);

                }
            })
            , pageHeader : React.createClass({
                 render : function () {
                     console.log("Rendering pageHeader");
                    return (
			                    <div>
	                    		{this.props.model.get('elements').map(function (child, key) {
			                        var level = this.props.level;
			                        var childLevel = level + 1;
			                        var name = child.get('name') || "";
			                        var style = this.props.viewStyle;
				                var classNames = "page-header";
					        var Comp = "h" + level;
			                        if ((key + 1) % 2 > 0)
			                        {
			                        	classNames = classNames + "-odd";
			                        } else {
			                        	classNames = classNames + "-even";
			                        }
                              if(getVisibility(child, this.props.target)) {
	        		                return (<div className={classNames}>
							<Comp  dangerouslySetInnerHTML={ {__html: marked(name.toString())}} />
							                          <hr/><ComponentView model={child} viewStyle={style} level={childLevel} target={this.props.target}/>
							                        </div>);
                              } else {
                                  return null;
                              }
		   			    }, this)}
			</div>);

                }
            })
           , sectionHeader : React.createClass({
                render : function () {
                    console.log("Rendering sectionHeader");
                    return (
			<div>
	                    		{this.props.model.get('elements').map(function (child, key) {
			                        var level = this.props.level;
			                        var childLevel = level + 1;
			                        var name = child.get('name') || "";
			                        var style = this.props.viewStyle;
				                var classNames = "do-not-break";
					        var Comp = "h" + level;
			                        if ((key + 1) % 2 > 0)
			                        {
			                        	classNames = classNames + " odd";
			                        } else {
			                        	classNames = classNames + " even";
			                        }
                              if (getVisibility(child, this.props.target)) {
	        		                return (<div className={classNames}>
							<Comp  dangerouslySetInnerHTML={ {__html: marked(name.toString())}} />
							                          <p><ComponentView model={child} viewStyle={style} level={childLevel} target={this.props.target}/></p>
							                        </div>);
                              } else {
                                  return null;
                              }
		   			    }, this)}
			</div>);
                }
            })
             , bubble : React.createClass({
                render : function () {
                    console.log("Rendering tooltip");
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var level = this.props.level;
                        var childLevel = level + 1;
                        var name = child.get('name') || "";
                        return (<div dangerouslySetInnerHTML={ {__html: marked(name.toString())}} />);
                    }, this);
                    return <div className="invisible-on-print">{nodes}</div>;
                }
            })
            , none : React.createClass({ 
                render : function () {
                    console.log("Rendering none");
		    var elements = this.props.model.get('elements');
  	            var level = this.props.level;
                    var Comp = "h" + level;
		    if (elements.length > 0)
		    {
	                    return (
				<div>
					{elements.map(function (child, key) {
		        	                var childLevel = level + 1;
		        	                var name = child.get('name') || "";
		        	                var style = this.props.viewStyle;
			                        //var Comp = React.DOM["h" + level];
						//var markup = '\u003c' + Comp + '\u003e' + marked(name.toString()) + '\u003c' + '\u002f' + Comp + '\u003e';
						  var markup =marked(name.toString());
              if (getVisibility(child, this.props.target)) {
	                        		return (<div>
								<Comp dangerouslySetInnerHTML={ {__html: markup}} />
								                        <ComponentView model={child} viewStyle={style} level={childLevel} key={level + "-" + 	key + "-2"} target={this.props.target}/>
							                        </div>);
              } else {
                  return null;
              }
		                    		}, this)}
				</div>);
		    }
		    else
		    {
		    	return <div key={level + "-term"} />;
		    }
                }
            })
        }
    });
    app.TextView = Backbone.View.extend({
        el : "#CV"
        , initialize : function () {
            console.log("Initializing model:" + this.model || "undefinded");
            this.listenTo(this.model, 'change', this.render);
        }
        , render : function (viewStyle, target) {
            viewStyle = viewStyle || "page";
            target = target || "it";
            console.log("Rendering element : " + $(this.el));
	          var element = <ComponentView model={this.model} viewStyle={viewStyle} target={target} key='0-0'/>;
            ReactDOM.render(element
                , this.$el.get(0)
		, function () {
        console.log("showing popups.....");
                	$(".haspopover").popover({live : true
						, html : true
	            				, placement : "auto"
	            				, trigger : "hover"
	            				, delay : 0
	            				});
	        });
            return this;
        }
    });
});
