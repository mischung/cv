"use strict";
var app = app || {};

$(function () {

    var showdown = new Showdown.converter();
    var getDateString = function (date) {
        //parse date from numbers e.g. 23-01-2012 to 23 Janurary, 2012, for 00-01-2012 the day value is ignored. Note: Does not check for correct day for that month. Assumes values are integers. If year is 0000 then the string "Now" is returned.
        var result = ""; //Output of string e.g. 10 January, 2019
        var splitDateNumbers; //array of date numbers in Day-Month-Year
        var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        var singleDigit, tenthDigit;
        if (date && _.isFunction(date.split)) {
                splitDateNumbers = date.split("-");
                if (Number(splitDateNumbers[0]) > 0) {
                    result = result + Number(splitDateNumbers[0]);
                    singleDigit = Number(splitDateNumbers[0].substr(splitDateNumbers[0].length - 1, 1));
                    tenthDigit = Number(splitDateNumbers[0].substr(splitDateNumbers[0].length - 2, 1)); 
                    if (singleDigit === 1 && tenthDigit !== 1) {
                        result = result + "st";
                    } else if (singleDigit === 2 && tenthDigit !== 1) {
                        result = result + "nd";
                    } else if (singleDigit === 2 && tenthDigit !== 1) {
                        result = result + "nd";
                    } else if (singleDigit === 3 && tenthDigit !== 1) {
                        result = result + "rd";
                    } else {
                        result = result + "th";
                    };
                };
                
                if (Number(splitDateNumbers[1]) > 0) {
                    result = result + " " + months[Number(splitDateNumbers[1] - 1)] + ", ";
                };
                if (Number(splitDateNumbers[2]) > 0) {
                    result = result + Number(splitDateNumbers[2]);
                } else if (Number(splitDateNumbers[2]) === 0) {
                    result = "present";
                };
        }
        return result;    
    }
    var componentView = React.createClass({
        getInitialState : function () {
            return {
                visible : true       
            }
        }
        , render : function () {
            var comp;
            var viewStyle = this.props.viewStyle || "app";
            var compObj = this.props.model.get('viewComponent') || {};
            var componentName = compObj[viewStyle] || "none"; 
            var level = this.props.level || 1; //level of node
            var model = this.props.model;
            comp = this.components[componentName] || this.components["none"];
            return <comp model={model} viewStyle={viewStyle} level={level} />
        }
        , components : { textBox : React.createClass({ 
                render : function () {
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var level = this.props.level;
                        var childLevel = level + 1;
                        var name = child.get('name') || "";
                        var style = this.props.viewStyle;
                        var headerTag = React.DOM["h" + level]; 
                        return <div><headerTag><span dangerouslySetInnerHTML={{__html : showdown.makeHtml(name.toString())}} /><componentView model={child} viewStyle={style} level={childLevel} /></headerTag></div>
                    }, this);
                    return <div>{nodes}</div>;
                }
            })
            , panel : React.createClass({
            	render : function () {
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var level = this.props.level;
                        var childLevel = level + 1;
                        var name = child.get('name') || "";
                        var panelHeader = (<div />);
                        var panelBody = (<div />);
                        var style = this.props.viewStyle;
                        var headerTag = React.DOM["h" + level]; 
                        if (name !== "") {
                        	panelHeader = (<div className="panel-heading"><headerTag><span dangerouslySetInnerHTML={{__html : showdown.makeHtml(name.toString())}} /></headerTag></div>);
                        }
                        if (child.get('elements') && child.get('elements').length > 0 ) {
                        	panelBody = (<div className="panel-body"><componentView model={child} viewStyle={style} level={childLevel} /></div>);
                        }
                        return <div className="panel panel-default">{panelHeader}{panelBody}</div>
                    }, this);
                    return <div>{nodes}</div>
            	}
            })
            , navTabs : React.createClass({ 
                render : function () {
                    console.log("Rendering navTabs")
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var childLevel = this.props.level + 1;
                        var idName = child.cid;
                        var classParam = "tab-pane fade";
                        var style = this.props.viewStyle;
                        if (key === 0) {
                            classParam = "tab-pane fade in active";
                        }
                        return <div className={classParam} id={idName}><componentView model={child} viewStyle={style} level={childLevel} /> </div>
                    }, this);
                    var names = this.props.model.get('elements').map(function (child, key) {
                        var name = child.get('name') || "";
                        var classParam = "";
                        var ref = "#" + child.cid;
                        if (key === 0) {
                            classParam = "active";
                        }

                        return (<li className={classParam}><a href={ref} data-toggle="tab" dangerouslySetInnerHTML={{__html : showdown.makeHtml(name.toString())}} /></li>);
                    }, this);
                    console.log("Got names: " + names);
                    return (
                    <div>
                        <ul className="nav nav-tabs">
                            {names}
                        </ul>
                        <div className="tab-content">
                            {nodes}
                        </div>
                    </div>
                    );
                }
            })
            , timeList : React.createClass({ 
                render : function () {
                    console.log("Rendering timeList")
                    var nodes = this.props.model.get('elements').map(function (child, key) {                    
                        var childLevel = this.props.level + 1;
                        var name = child.get('name') || "";
                        var style = this.props.viewStyle;
                        var dateFrom = getDateString(child.get('timeStart')) || "";
                        var dateTo = getDateString(child.get('timeEnd')) || "";
                        var dateString;
                        if (dateFrom.length > 0) {
                            dateString = dateFrom + " to " + dateTo;
                        } else {
                            dateString = dateTo;
                        };
                        return (<div><dt>{dateString}</dt><dd><span dangerouslySetInnerHTML={{__html : showdown.makeHtml(name.toString())}} /><componentView model={child} viewStyle={style} level={childLevel} /></dd></div>);
                    }, this);
                    return (<dl className="dl-horizontal">{nodes}</dl>);
                }
            })
            , list : React.createClass({ 
                render : function () {
                    console.log("Rendering list")
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var childLevel = this.props.level + 1;
                        var name = child.get('name') || "";
                        var style = this.props.viewStyle;
                        return (<li><span className="list-group-items" dangerouslySetInnerHTML={{__html : showdown.makeHtml(name.toString())}} /><componentView model={child} viewStyle={style} level={childLevel} /></li>);
                    }, this);
                    return (<div><ul className="list-group">{nodes}</ul></div>);
                }
            })
            , inlineList : React.createClass({
                render : function () {
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var childLevel = this.props.level + 1;
                        var name = child.get('name') || "";
						var content;            		
						React.renderComponentToString(<componentView model={child} level={childLevel} className="invisible-on-print"/>
							, function (html) {
							content = html
							console.log("Rendering the HTML: " + html);
		           	 	});
                        return (<li className="btn-group">
                        			<button id={child.cid} type="button"
                        				data-container="body" 
                        				data-toggle="popover"
                        				data-placement="bottom" 
                        				data-content={content}	
                        				className="btn btn-info haspopover"
                        				dangerouslySetInnerHTML={{__html : showdown.makeHtml(name.toString())}}/>
                        		</li>);
                    }, this);
                    return (<ul className="list-inline">{nodes}</ul>	);
                }
            })
            , pageHeader : React.createClass({ 
                render : function () {
                    console.log("Rendering pageHeader")
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var level = this.props.level;
                        var childLevel = level + 1;
                        var name = child.get('name') || "";
                        var style = this.props.viewStyle;
                        var comp = React.DOM["h" + level];
                        var classNames = "page-header";
                        if ((key + 1) % 2 > 0)
                        {
                        	classNames = classNames + " odd";
                        } else {
                        	classNames = classNames + " even";
                        }
                        return (<section className={classNames}><comp dangerouslySetInnerHTML={{__html : showdown.makeHtml(name.toString())}} /><p><componentView model={child} viewStyle={style} level={childLevel} /></p></section>);
                    }, this);
                    return (<div>{nodes}</div>);
                }
            })
            , sectionHeader : React.createClass({ 
                render : function () {
                    console.log("Rendering pageHeader")
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var level = this.props.level;
                        var childLevel = level + 1;
                        var name = child.get('name') || "";
                        var style = this.props.viewStyle;
                        var comp = React.DOM["h" + level];
                        var classNames = "do-not-break";
                        if ((key + 1) % 2 > 0)
                        {
                        	classNames = classNames + " odd";
                        } else {
                        	classNames = classNames + " even";
                        }
                        return (<div className={classNames}><comp dangerouslySetInnerHTML={{__html : showdown.makeHtml(name.toString())}} /><p><componentView model={child} viewStyle={style} level={childLevel} /></p></div>);
                    }, this);
                    return (<div>{nodes}</div>);
                }
            })
             , bubble : React.createClass({
                render : function () {
                    console.log("Rendering none")
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var level = this.props.level;
                        var childLevel = level + 1;
                        var name = child.get('name') || "";
                        var comp = React.DOM["h" + level];
                        return (<div dangerouslySetInnerHTML={{__html : showdown.makeHtml(name.toString())}} />);
                    }, this);
                    return <div className="invisible-on-print">{nodes}</div>;
                }
            })
            , none : React.createClass({ 
                render : function () {
                    console.log("Rendering none")
                    var nodes = this.props.model.get('elements').map(function (child, key) {
                        var level = this.props.level;
                        var childLevel = level + 1;
                        var name = child.get('name') || "";
                        var style = this.props.viewStyle;
                        var comp = React.DOM["h" + level];
                        return (<div><comp>{mark(name.toString())}</comp><componentView model={child} viewStyle={style} level={childLevel} /></div>);
                    }, this);
                    return <div>{nodes}</div>;
                }
            })
        }
    });
    app.TextView = Backbone.View.extend({
        el : "#CV"
        , initialize : function () {
            console.log("Initializing model:" + this.model || "undefinded");
            this.listenTo(this.model, 'change', this.render);
        }
        , render : function (viewStyle) {
            viewStyle = viewStyle || "page";
            console.log("Rendering element : " + $(this.el));
            ReactDOM.renderComponent(< componentView
                model={this.model} viewStyle={viewStyle} />
                , this.$el.get(0)
                , function () {
                	console.log("showing popups.....")
                	$(".haspopover").popover({live : true
	            								, html : true
	            								, placement : "auto bottom"
	            								, trigger : "focus hover"
	            								, delay : 0
	            								});
	            });
            return this;
        }
    });




});
