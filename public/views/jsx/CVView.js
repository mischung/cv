
"use strict";
var app = app || {};

$(function () {
    
    app.CVView = Backbone.View.extend({
        el : '#CV'
        , initialize : function () {
            console.log("Initializing model:" + this.model || "undefinded");
            this.listenTo(this.model, 'change', this.render);
        }
        , render : function () {
            console.log("Rendering element : " + $(this.el));
            $(this.el).html(JSON.stringify(this.model));
        }
    });
});
