"use strict";

var app = app || {};
$(function () {
  var getDateString = function getDateString(date) {
    //parse date from numbers e.g. 23-01-2012 to 23 Janurary, 2012, for 00-01-2012 the day value is ignored. Note: Does not check for correct day for that month. Assumes values are integers. If year is 0000 then the string "Now" is returned.
    var result = ""; //Output of string e.g. 10 January, 2019

    var splitDateNumbers; //array of date numbers in Day-Month-Year

    var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var singleDigit, tenthDigit;

    if (date && _.isFunction(date.split)) {
      splitDateNumbers = date.split("-");

      if (Number(splitDateNumbers[0]) > 0) {
        result = result + Number(splitDateNumbers[0]);
        singleDigit = Number(splitDateNumbers[0].substr(splitDateNumbers[0].length - 1, 1));
        tenthDigit = Number(splitDateNumbers[0].substr(splitDateNumbers[0].length - 2, 1));

        if (singleDigit === 1 && tenthDigit !== 1) {
          result = result + "st";
        } else if (singleDigit === 2 && tenthDigit !== 1) {
          result = result + "nd";
        } else if (singleDigit === 2 && tenthDigit !== 1) {
          result = result + "nd";
        } else if (singleDigit === 3 && tenthDigit !== 1) {
          result = result + "rd";
        } else {
          result = result + "th";
        }

        ;
      }

      ;

      if (Number(splitDateNumbers[1]) > 0) {
        result = result + " " + months[Number(splitDateNumbers[1] - 1)] + ", ";
      }

      ;

      if (Number(splitDateNumbers[2]) > 0) {
        result = result + Number(splitDateNumbers[2]);
      } else if (Number(splitDateNumbers[2]) === 0) {
        result = "present";
      }

      ;
    }

    return result;
  };

  var getVisibility = function getVisibility(child, target) {
    var result = true;
    var targetKey = target || "all";
    var comp = child.get('viewComponent') || {};
    var targetVisibility = comp[targetKey] || "visible";

    if (targetVisibility === "hidden") {
      result = false;
    } else if (targetVisibility === "visible") {
      result = true;
    }

    return result;
  };

  var ComponentView = React.createClass({
    displayName: "ComponentView",
    getInitialState: function getInitialState() {
      var target = this.props.target || "all";
      return {
        visible: true,
        target: target
      };
    },
    render: function render() {
      var viewStyle = this.props.viewStyle || "app";
      var compObj = this.props.model.get('viewComponent') || {};
      var componentName = compObj[viewStyle] || "none";
      var level = this.props.level || 1; //level of node

      var model = this.props.model;
      var Comp = this.components[componentName] || this.components["none"];
      var target = this.props.target || "all";
      return React.createElement(Comp, {
        model: model,
        viewStyle: viewStyle,
        level: level,
        target: target
      });
    },
    components: {
      textBox: React.createClass({
        displayName: "textBox",
        render: function render() {
          var nodes = this.props.model.get('elements').map(function (child, key) {
            var level = this.props.level;
            var childLevel = level + 1;
            var name = child.get('name') || "";
            var style = this.props.viewStyle;
            var HeaderTag = "h" + level;

            if (getVisibility(child, this.props.target)) {
              return React.createElement("div", null, React.createElement(HeaderTag, null, React.createElement("span", {
                dangerouslySetInnerHTML: {
                  __html: marked(name.toString())
                }
              }), React.createElement(ComponentView, {
                model: child,
                viewStyle: style,
                level: childLevel,
                target: this.props.target
              })));
            } else {
              return null;
            }
          }, this);
          return React.createElement("div", null, nodes);
        }
      }),
      panel: React.createClass({
        displayName: "panel",
        render: function render() {
          var nodes = this.props.model.get('elements').map(function (child, key) {
            var level = this.props.level;
            var childLevel = level + 1;
            var name = child.get('name') || "";
            var panelHeader = React.createElement("div", null);
            var panelBody = React.createElement("div", null);
            var style = this.props.viewStyle;
            var HeaderTag = "h" + level;

            if (name !== "") {
              panelHeader = React.createElement("div", {
                className: "panel-heading"
              }, React.createElement(HeaderTag, null, React.createElement("span", {
                dangerouslySetInnerHTML: {
                  __html: marked(name.toString())
                }
              })));
            }

            if (child.get('elements') && child.get('elements').length > 0) {
              panelBody = React.createElement("div", {
                className: "panel-body"
              }, React.createElement(ComponentView, {
                model: child,
                viewStyle: style,
                level: childLevel,
                target: this.props.target
              }));
            }

            if (getVisibility(child, this.props.target)) {
              return React.createElement("div", {
                className: "panel panel-default"
              }, panelHeader, panelBody);
            } else {
              return null;
            }
          }, this);
          return React.createElement("div", null, nodes);
        }
      }),
      navTabs: React.createClass({
        displayName: "navTabs",
        render: function render() {
          console.log("Rendering navTabs");
          var nodes = this.props.model.get('elements').map(function (child, key) {
            var childLevel = this.props.level + 1;
            var idName = child.cid;
            var classParam = "tab-pane fade";
            var style = this.props.viewStyle;

            if (key === 0) {
              classParam = "tab-pane fade in active";
            }

            if (getVisibility(child, this.props.target)) {
              return React.createElement("div", {
                className: classParam,
                id: idName
              }, React.createElement(ComponentView, {
                model: child,
                viewStyle: style,
                level: childLevel,
                target: this.props.target
              }));
            } else {
              return null;
            }
          }, this);
          var names = this.props.model.get('elements').map(function (child, key) {
            var name = child.get('name') || "";
            var classParam = "";
            var ref = "#" + child.cid;

            if (key === 0) {
              classParam = "active";
            }

            if (getVisibility(child, this.props.target)) {
              return React.createElement("li", {
                className: classParam
              }, React.createElement("a", {
                href: ref,
                "data-toggle": "tab",
                dangerouslySetInnerHTML: {
                  __html: marked(name.toString())
                }
              }));
            } else {
              return null;
            }
          }, this);
          console.log("Got names: " + names);
          return React.createElement("div", null, React.createElement("ul", {
            className: "nav nav-tabs"
          }, names), React.createElement("div", {
            className: "tab-content"
          }, nodes));
        }
      }),
      timeList: React.createClass({
        displayName: "timeList",
        render: function render() {
          console.log("Rendering timeList");
          var nodes = this.props.model.get('elements').map(function (child, key) {
            var childLevel = this.props.level + 1;
            var name = child.get('name') || "";
            var style = this.props.viewStyle;
            var dateFrom = getDateString(child.get('timeStart')) || "";
            var dateTo = getDateString(child.get('timeEnd')) || "";
            var dateString;

            if (dateFrom.length > 0) {
              dateString = dateFrom + " to " + dateTo;
            } else {
              dateString = dateTo;
            }

            ;

            if (getVisibility(child, this.props.target)) {
              return React.createElement("div", {
                className: "row"
              }, React.createElement("div", {
                className: "time-column"
              }, dateString), React.createElement("div", {
                className: "event-column"
              }, React.createElement("span", {
                dangerouslySetInnerHTML: {
                  __html: marked(name.toString())
                }
              }), React.createElement(ComponentView, {
                model: child,
                viewStyle: style,
                level: childLevel,
                target: this.props.target
              })));
            } else {
              return null;
            }
          }, this);
          return React.createElement("div", {
            className: "dl-horizontal"
          }, nodes);
        }
      }),
      list: React.createClass({
        displayName: "list",
        render: function render() {
          console.log("Rendering list");
          var nodes = this.props.model.get('elements').map(function (child, key) {
            var childLevel = this.props.level + 1;
            var name = child.get('name') || "";
            var style = this.props.viewStyle;

            if (getVisibility(child, this.props.target)) {
              return React.createElement("li", null, React.createElement("div", {
                className: "list-group-items",
                dangerouslySetInnerHTML: {
                  __html: marked(name.toString())
                }
              }), React.createElement(ComponentView, {
                model: child,
                viewStyle: style,
                level: childLevel,
                target: this.props.target
              }));
            } else {
              return null;
            }
          }, this);
          return React.createElement("div", null, React.createElement("ul", {
            className: "list-group"
          }, nodes));
        }
      }),
      inlineList: React.createClass({
        displayName: "inlineList",
        render: function render() {
          var nodes = this.props.model.get('elements').map(function (child, key) {
            var childLevel = this.props.level + 1;
            var name = child.get('name') || "";
            var content;
            content = child.get('elements').map(function (el) {
              return marked(el.get('name'));
            }).join(",") || ""; //			React.renderToString(<ComponentView model={child} level={childLevel} className="invisible-on-print"/>
            //				, function (html) {
            //				content = html
            //				console.log("Rendering the HTML: " + html);
            //   	 	}, this);

            return React.createElement("li", {
              className: "btn-group"
            }, React.createElement("button", {
              id: child.cid,
              type: "button",
              "data-container": "body",
              "data-placement": "bottom",
              className: "btn btn-info haspopover",
              "data-toggle": "popover",
              "data-content": content,
              dangerouslySetInnerHTML: {
                __html: marked(name.toString())
              }
            }));
          }, this);
          return React.createElement("ul", {
            className: "list-inline"
          }, nodes);
        }
      }),
      titleHeader: React.createClass({
        displayName: "titleHeader",
        render: function render() {
          console.log("Rendering titleHeader");
          return React.createElement("div", {
            className: "row title-header"
          }, this.props.model.get('elements').map(function (child, key) {
            var level = this.props.level;
            var childLevel = level + 1;
            var name = child.get('name') || "";
            var style = this.props.viewStyle;
            var classNames = "column";
            var Comp = "h" + level;

            if ((key + 1) % 2 > 0) {
              classNames = classNames + "-left";
            } else {
              classNames = classNames + "-right";
            }

            if (getVisibility(child, this.props.target)) {
              return React.createElement("div", {
                className: classNames
              }, React.createElement(Comp, {
                dangerouslySetInnerHTML: {
                  __html: marked(name.toString())
                }
              }), React.createElement("p", null, React.createElement(ComponentView, {
                model: child,
                viewStyle: style,
                level: childLevel,
                target: this.props.target
              })));
            } else {
              return null;
            }
          }, this));
        }
      }),
      pageHeader: React.createClass({
        displayName: "pageHeader",
        render: function render() {
          console.log("Rendering pageHeader");
          return React.createElement("div", null, this.props.model.get('elements').map(function (child, key) {
            var level = this.props.level;
            var childLevel = level + 1;
            var name = child.get('name') || "";
            var style = this.props.viewStyle;
            var classNames = "page-header";
            var Comp = "h" + level;

            if ((key + 1) % 2 > 0) {
              classNames = classNames + "-odd";
            } else {
              classNames = classNames + "-even";
            }

            if (getVisibility(child, this.props.target)) {
              return React.createElement("div", {
                className: classNames
              }, React.createElement(Comp, {
                dangerouslySetInnerHTML: {
                  __html: marked(name.toString())
                }
              }), React.createElement("hr", null), React.createElement(ComponentView, {
                model: child,
                viewStyle: style,
                level: childLevel,
                target: this.props.target
              }));
            } else {
              return null;
            }
          }, this));
        }
      }),
      sectionHeader: React.createClass({
        displayName: "sectionHeader",
        render: function render() {
          console.log("Rendering sectionHeader");
          return React.createElement("div", null, this.props.model.get('elements').map(function (child, key) {
            var level = this.props.level;
            var childLevel = level + 1;
            var name = child.get('name') || "";
            var style = this.props.viewStyle;
            var classNames = "do-not-break";
            var Comp = "h" + level;

            if ((key + 1) % 2 > 0) {
              classNames = classNames + " odd";
            } else {
              classNames = classNames + " even";
            }

            if (getVisibility(child, this.props.target)) {
              return React.createElement("div", {
                className: classNames
              }, React.createElement(Comp, {
                dangerouslySetInnerHTML: {
                  __html: marked(name.toString())
                }
              }), React.createElement("p", null, React.createElement(ComponentView, {
                model: child,
                viewStyle: style,
                level: childLevel,
                target: this.props.target
              })));
            } else {
              return null;
            }
          }, this));
        }
      }),
      bubble: React.createClass({
        displayName: "bubble",
        render: function render() {
          console.log("Rendering tooltip");
          var nodes = this.props.model.get('elements').map(function (child, key) {
            var level = this.props.level;
            var childLevel = level + 1;
            var name = child.get('name') || "";
            return React.createElement("div", {
              dangerouslySetInnerHTML: {
                __html: marked(name.toString())
              }
            });
          }, this);
          return React.createElement("div", {
            className: "invisible-on-print"
          }, nodes);
        }
      }),
      none: React.createClass({
        displayName: "none",
        render: function render() {
          console.log("Rendering none");
          var elements = this.props.model.get('elements');
          var level = this.props.level;
          var Comp = "h" + level;

          if (elements.length > 0) {
            return React.createElement("div", null, elements.map(function (child, key) {
              var childLevel = level + 1;
              var name = child.get('name') || "";
              var style = this.props.viewStyle; //var Comp = React.DOM["h" + level];
              //var markup = '\u003c' + Comp + '\u003e' + marked(name.toString()) + '\u003c' + '\u002f' + Comp + '\u003e';

              var markup = marked(name.toString());

              if (getVisibility(child, this.props.target)) {
                return React.createElement("div", null, React.createElement(Comp, {
                  dangerouslySetInnerHTML: {
                    __html: markup
                  }
                }), React.createElement(ComponentView, {
                  model: child,
                  viewStyle: style,
                  level: childLevel,
                  key: level + "-" + key + "-2",
                  target: this.props.target
                }));
              } else {
                return null;
              }
            }, this));
          } else {
            return React.createElement("div", {
              key: level + "-term"
            });
          }
        }
      })
    }
  });
  app.TextView = Backbone.View.extend({
    el: "#CV",
    initialize: function initialize() {
      console.log("Initializing model:" + this.model || "undefinded");
      this.listenTo(this.model, 'change', this.render);
    },
    render: function render(viewStyle, target) {
      viewStyle = viewStyle || "page";
      target = target || "it";
      console.log("Rendering element : " + $(this.el));
      var element = React.createElement(ComponentView, {
        model: this.model,
        viewStyle: viewStyle,
        target: target,
        key: "0-0"
      });
      ReactDOM.render(element, this.$el.get(0), function () {
        console.log("showing popups.....");
        $(".haspopover").popover({
          live: true,
          html: true,
          placement: "auto",
          trigger: "hover",
          delay: 0
        });
      });
      return this;
    }
  });
});
