 #!/bin/bash

#Installs heroku to use Heroku buildback
if [ -z "$1"]
	then
		heroku create
	else
		heroku git:remote -a $1
fi

#Login to heroku account and add keys
heroku login
#heroku config:add BUILDPACK_URL=https://github.com/arunoda/heroku-nodejs-binary-buildback.git
#heroku labs:enable user-env-compile #one time action
#heroku config:add NODE_VERSION="0.10.21"
#set number of processes
heroku ps:scale web=1
