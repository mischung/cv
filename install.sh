#!/bin/bash
# install newer node
sudo apt-get install --yes python-software-properties 
sudo add-apt-repository ppa:chris-lea/node.js 
sudo apt-get update 
sudo apt-get upgrade
sudo apt-get install --yes nodejs redis-server

sudo apt-get install --yes build-essential;
sudo apt-get install curl screen

#install Heroku toolbelt
wget -qO- https://toolbelt.heroku.com/install-ubuntu.sh | sh

#clone repo to this machine
#git clone https://bitbucket.org/mischung/CV

./configHeroku.sh CV

#install npm components
npm install express
npm install express-dot
sudo npm install jslint -g
sudo npm install -g react-tools
jsx --watch ./ ./public ./public/views/
