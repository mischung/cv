exports.db = [{
    viewComponent: {
        app: "panel",
        page: "panel"
    }

    ,
    elements: [{
        viewComponent: {
            app: "titleHeader",
            page: "titleHeader"
        },
        name: "",
        elements: [{
            name: "<big><big>**Martin Chung**</big></big><br/><small>PhD MSc BSc(Hons) HKMLT(Part II)</small>"
            }, {
                name: "<h3><small>Toronto, Ontario, Canada<br/>**Mobile:** 1-647-853-8382<br/>**E-mail:** [mcaddint@gmail.com](mailto:mcaddint@gmail.com)<br/>**Linkedin:** [https://www.linkedin.com/in/martin-chung-3219713a](https://www.linkedin.com/in/martin-chung-3219713a)<br/>**Web Page(IT):** [https://chunginshing-cv.herokuapp.com/CV-it](https://chunginshing-cv.herokuapp.com/CV-it)<br/>**Web Page(Biotech):** [https://chunginshing-cv.herokuapp.com/CV-biotech](https://chunginshing-cv.herokuapp.com/CV-biotech)</small></h3>"
        }]
    }, {
        viewComponent: {
            app: "navTabs",
            page: "pageHeader"
        },
        name: "",
        elements: [{
                name: "Summary",
                viewComponent: {
                    app: "list",
                    page: "list",
                    it: "visible",
                    biotech: "hidden"
                },
                elements: [{
                    name:"4+ years of professional experience in developing C/C++ software with 2+ years in embedded Linux development."
                },{
                    name:"Proven ability in building multi-threaded and multi-platform C++ programs."
                },{
                    name:"Experience in prototyping (in Python), implementing and optimizing algorithms in machine vision and SLAM."
                },{
                    name:"Hands on knowledge in VoIP signalling and media (e.g. SIP, STUN, TURN, ICE)."
                },{
                    name:"Knowledge in the use of code review (Gerrit), source control (Git), testing (Google Test), continuous delivery and integration (Jenkins/GitLab)."
                },{
                    name:"Deep knowledge in molecular biology and next generation sequencing."
                }]
                },{
                name: "Summary",
                viewComponent: {
                    app: "list",
                    page: "list",
                    it: "hidden",
                    biotech: "visible"
                },
                elements: [{
                    name:"3+ years experience in nucleic acid assay research and development."
                },{
                    name:"Hands on experience in next generation sequencing experiment and analysis."
                },{
                    name:"Experience in developing software for diagnostic machines."
                },{
                    name:"4+ years of professional coding experience."
                }]
                },{
                name: "Work Experience",
                viewComponent: {
                    app: "timeList",
                    page: "timeList"
                },
                elements: [{
                    timeStart: "06-04-2020",
                    timeEnd: "00-00-0000",
                    timeFormat: "MM-YYYY",
                    name: "**Software Developer** \n\n ** IBM Canada Limited**",
                    viewComponent: {
                        app: "list",
                        page: "list"
                    },
                    elements: [{
                        viewComponent: {
                            it: "visible",
                            biotech: "visible"
                        },
			    name: "Porting Python and its packages to the z/OS."
                    }, {
                        viewComponent: {
                            it: "visible",
                            biotech: "visible"
                        },
			    name: "Skills: C, Python, z/OS, Buildbot  and Git."
                    }]
                }, {
                    timeStart: "00-08-2017",
                    timeEnd: "00-02-2019",
                    timeFormat: "MM-YYYY",
                    name: "**Senior Software Engineer** \n\n **M800 Limited**",
                    viewComponent: {
                        app: "list",
                        page: "list"
                    },
                    elements: [{
                        viewComponent: {
                            it: "visible",
                            biotech: "visible"
                        },
                        name: "Developed a VoIP SDK based on C++ for Android, iOS and Linux."
                    }, {
                        viewComponent: {
                            it: "visible",
                            biotech: "hidden"
                        },
                        name: "Developed a prototype based on our VoIP SDK running on client selected embedded Linux devices."
                    }, {
                        viewComponent: {
                            it: "visible",
                            biotech: "hidden"
                        },
                        name: "Developed cross-compiling environment, as a Docker image, for building the SDK."
                    }, {
                        viewComponent: {
                            it: "visible",
                            biotech: "visible"
                        },
                        name: "Skills: VoIP, WebRTC, embedded Linux, C++, Docker, Google Test, Jenkins/GitLab and Git."
                    }]
                }, {
                    timeStart: "00-10-2016",
                    timeEnd: "00-08-2017",
                    timeFormat: "MM-YYYY",
                    name: "**Software Engineer** \n\n **Ilife Innovation Limited**",
                    viewComponent: {
                        app: "list",
                        page: "list"
                    },
                    elements: [{
                        viewComponent: {
                            it: "visible",
                            biotech: "visible"
                        },
                        name: "Developed software for the first camera based vacuum cleaning robot in China."
                    }, {
                        viewComponent: {
                            it: "visible",
                            biotech: "hidden"
                        },
                        name: "Optimized machine vision/SLAM algorithms and code to run on ARM Cortex-A7 device."
                    }, {
                        viewComponent: {
                            it: "visible",
                            biotech: "hidden"
                        },
                        name: "Written camera calibration code in Python for the vacuum cleaning robot."
                    }, {
                        viewComponent: {
                            it: "visible",
                            biotech: "visible"
                        },
                        name: "Skills: machine vision, embedded Linux, C++, Python, OpenCV, Eigen, Git and Mantis."
                    }]
                }, {
                    timeStart: "00-10-2011",
                    timeEnd: "00-07-2015",
                    timeFormat: "MM-YYYY",
                    name: "**Research Scientist** \n\n **Research and Development, DiagCor Bioscience Incorporation Limited**",
                    viewComponent: {
                        app: "list",
                        page: "list"
                    },
                    elements: [{
                        name: "Established IT/bioinformatics workflow for the next generation sequencing service."
                    }, {
                        name: "Developed firmware for our assay equipment."
                    }, {
                        name: "Developed software and algorithms for our camera based data acquisition system."
                    }, {
                        name: "Provided wet lab support for NGS runs on Illumina MiSeq."
                    }, {
                        name: "Developed an assay for screening multi-drug resistant *mycobacterium tuberculosis*."
                    }, {
                        name: "Developed a DNA barcoding technology for an in-house assay product."
                    }, {
                        name: "Skills: machine vision, C, Galaxy and BaseSpace."
                    }]
                }, {
                    timeStart: "00-07-2007",
                    timeEnd: "00-12-2007",
                    timeFormat: "MM-YYYY",
                    name: "**Research Assistant** \n\n **Department of Medicine, University of Hong Kong**",
                    viewComponent: {
                        app: "list",
                        page: "list"
                    },
                    elements: [{
                        name: "Discovered the role of Sox group F genes in blood cell development using zebrafish as a model organism."
                    }, {
                        name: "Skills: zebrafish, microinjection, cloning, flow-cytometry and confocal microscopy."
                    }]
                }, {
                    timeStart: "00-01-2007",
                    timeEnd: "00-05-2007",
                    timeFormat: "MM-YYYY",
                    name: "**Sales representative** \n\n **Seekers Biomedical Limited, Hong Kong**",
                    viewComponent: {
                        app: "list",
                        page: "list"
                    },
                    elements: [{
                        name: "Promoted medical diagnostics to hospitals and clinics in Hong Kong."
                    }]
                }]
            }, {
                name: "Education",
                viewComponent: {
                    app: "timeList",
                    page: "timeList"
                },
                elements: [{
                    timeStart: "00-00-2008",
                    timeEnd: "00-00-2011",
                    timeFormat: "YYYY",
                    name: "**PhD, Department of Medicine, University of Hong Kong.**"
                }, {
                    timeStart: "00-00-2003",
                    timeEnd: "00-00-2006",
                    timeFormat: "YYYY",
                    name: "**MSc, Department of Biochemistry, University of Toronto.**"
                }, {
                    timeStart: "00-00-1999",
                    timeEnd: "00-00-2003",
                    timeFormat: "YYYY",
                    name: "**Hons. BSc, Department of Biochemistry, University of Toronto (GPA: 3.40).**"
                }]
            }, {
                name: "Personal Software/Hardware Projects",
                viewComponent: {
                    app: "timeList",
                    page: "timeList",
                    it: "visible",
                    biotech: "hidden"
                },
                elements: [{
                    timeStart: "00-11-2013",
                    timeFormat: "MM-YYYY",
                    timeEnd: "00-00-0000",
                    name: "**Personal CV.**",
                    viewComponent: {
                        app: "list",
                        page: "list"
                    },
                    elements: [{
                        name: "Personal CV hosted on Heroku."
                              }, {
                        name: "Skills: JavaScript, NodeJS, Heroku, React and JSX."
                    }, {
                        name: "[<u>Source code</u>](https://bitbucket.org/mischung/cv)"
                    }]

                }, {
                    timeStart: "00-01-2016",
                    timeFormat: "MM-YYYY",
                    timeEnd: "00-07-2016",
                    name: "**Structured light based mobile 3D scanner using Raspberry Pi.**",
                    viewComponent: {
                        app: "list",
                        page: "list"
                    },
                    elements: [{
                        name: "Produced a proof-of-concept structured light, stereo camera 3D scanner capable of three shot dense reconstruction and a single shot structured light single camera scanner for sparse reconstruction."
                              }, {
                        name: "Skills: structured light scanning, machine vision, Linux and C++."
                    }, {
                        name: "[<u>High-res model</u>](https://skfb.ly/QTtP), [<u>Low-res model</u>](https://skfb.ly/NySP), [<u>Image1</u>](image1-1.jpg), [<u>Image2</u>](image1-2.jpg), [<u>Bitbucket repository</u>](https://bitbucket.org/mischung/structure_light_scanner)"
                    }]
                }, {
                    timeStart: "00-09-2015",
                    timeFormat: "MM-YYYY",
                    timeEnd: "00-12-2015",
                    name: "**Adaptive controller for a mini-quadcopter.**",
                    viewComponent: {
                        app: "list",
                        page: "list"
                    },
                    elements: [{
                        name: "Written an adaptive controller for the Crazyflie 2.0 quadcopter."
                    }, {
                        name: "Skills: adaptive controller, C, freeRTOS and Crazyflie."
                    }, {
                        name: "[<u>Bitbucket repository</u>](https://bitbucket.org/mischung/crazyflie)"
                    }]
                }, {
                    timeStart: "00-07-2015",
                    timeFormat: "MM-YYYY",
                    timeEnd: "00-9-2015",
                    name: "**Reinforcement learning for a mini-quadcopter.**",
                    viewComponent: {
                        app: "list",
                        page: "list"
                    },
                    elements: [{
                        name: "Written software to make the Crazyflie 2.0 quadcopter learn how to fly with the help of a Kinect 2.0."
                    }, {
                        name: "Skills: C, C#, computer vision, CrazyFlie and Kinect 2.0."
                    }, {
                        name: "[<u>Bitbucket repository</u>](https://bitbucket.org/mischung/rl_crazyflie)"
                    }]
                }, {
                    timeStart: "00-06-2011",
                    timeEnd: "00-01-2012",
                    timeFormat: "MM-YYYY",
                    name: "**[P! micro.](https://itunes.apple.com/us/app/p!-micro/id493854177?ls=1&mt=8)**",
                    viewComponent: {
                        app: "list",
                        page: "list"
                    },
                    elements: [{
                        name: "'P! micro' is an IOS app for applying 'touch-ups' to photos in real time, similar to Purikura machines. The software also simulates the glittering effects of certain materials when viewed from the screen."
                    }, {
                        name: "Skills: iOS, XCode, Objective C, OpenGL ES 2.0 and machine vision."
                    }]
                }]
            },
            {
                name: "Coding Exercises",
                viewComponent: {
                    app: "list",
                    page: "list",
                    it: "visible",
                    biotech: "hidden"
                },
                elements: [{
                    name: "**ROSALIND :** [<u>Profile</u>](http://rosalind.info/users/mcaddint/), [<u>Source code</u>](https://bitbucket.org/mischung/rosalind_exercises)"
                },{
                    name: "**HackerRank :**[<u>Profile</u>](https://www.hackerrank.com/mcaddint)"
                }]
            },
            /*    , {name : "Scientific Research Achievements"
                  , viewComponent : { app : "timeList"
                  , page : "timeList"
                  }
                  , elements : [ { timeStart : "00-00-2008"
                  , timeEnd  : "00-00-2011"
                  , timeFormat : "YYYY"
                  , name  : "Successfully characterized the hematopoietic and vascular functions of an important group of transcription factors in zebrafish, paving the way for possible study of these genes in human leukemia."}
                  , { timeStart : "00-00-2003"
                  , timeEnd  : "00-00-2006"
                  , timeFormat : "YYYY"
                  , name  : "Sequenced novel tropoelastin gene homologues in zebrafish. Compared the sequences with the mammalian and avian orthologues to characterize the evolution of this gene."}
                  ]} */
            /*		, {name : "Laboratory Skills"
            			, viewComponent : { app : "sectionHeader"
            				, page : "sectionHeader"
            			}
            			, elements : [ {name : "Next Generation Sequencing"
            				, viewComponent : { app : "inlineList"
            					, page : "inlineList"
            				}
            				, elements : [{name  : "Library Preparation using Illumina's Nextera kits"
            					, viewComponent : { app : "bubble"
            						, page : "bubble"
            					}
            					,elements : [{name : "Prepared libraries from human DNA for sequencing using the Nextera exome and inherited disease kits."}]}
            				,{name  : "Sequencing using Illumina's MiSeq platform"
            					, viewComponent : { app : "bubble"
            						, page : "bubble"
            					}
            					,elements : [{name : "Experience in the use and troubleshooting of sequencing runs using Illumina's MiSeq platform."}]}
            				]}
            			,{ name : "Imaging Techniques"
            				, viewComponent : { app : "inlineList"
            					, page : "inlineList"
            				}
            				,elements : [{name  : "Confocal microscopy"
            					, viewComponent : { app : "bubble"
            						, page : "bubble"
            					}
            					, elements : [{name : "Experience in the mounting of zebrafish embryos. Acquired knowledge in the analysis and processing of image data using ImageJ and Carl Zeiss AIM software."}]}
            				,{name  : "Whole mount *in situ* hybridization and immunostaining of zebrafish embryos"
            					, viewComponent : { app : "bubble"
            						, page : "bubble"
            					}
            					, elements : [{name : "Practical knowledge in whole mount staining and imaging of zebrafish embryos."}]}
            				]}
            			, { name : "Cellular Techniques"
            				, viewComponent : { app : "inlineList"
            					, page : "inlineList"
            				}
            				, elements : [{name  : "Flow cytometry and FACS sorting"
            					, viewComponent : { app : "bubble"
            						, page : "bubble"
            					}
            					, elements : [{name : "Extensive experience in flow cytometry for the analysis of zebrafish hematopoietic cells labeled with fluorescent markers. Demonstrated success in isolating hematopoietic cells from zebrafish embryos using FACS sorting and downstream processing of these cells for real time RT-PCR."}]}
            				, {name : "Micro-injection"
            					, viewComponent : { app : "bubble"
            						, page : "bubble"
            					}
            					, elements : [{name : "Hands on experience in the micro-injection of DNA/RNA and morpholinoes into zebrafish embryos for gene over-expression and knock-down experiments."}]}
            				, {name  : "Bacterial Culture"
            					, viewComponent : { app : "bubble"
            						, page : "bubble"
            					}
            					, elements : [{name : "Knowledge in bacterial culture of *e.coli* for molecular cloning."}]}
            				, {name  : "Cell cycle profiling"
            					, viewComponent : { app : "bubble"
            						, page : "bubble"
            					}
            					, elements : [{name : "Experience in the use of common DNA dyes (such as the live cell permeable dye DRAQ5 and impermeable dye PI) and the use of flow cytometry for cell cycle analysis."}]}
            				]}
            			, { name : "Molecular Techniques"
            				, viewComponent : { app : "inlineList"
            					, page : "inlineList"
            				}
            				, elements : [{name  : "Flow-through hybridization"
            					, viewComponent : { app : "bubble"
            						, page : "bubble"
            					}
            					, elements : [{name : "Expertise in the use of flow-through hybridization for the detection of target DNA. This technique is employed in most of DiagCors's diagnostic products."}]}
            				//		, {name  : "Molecular cloning"
            				//			, viewComponent : { app : "bubble"
            				//							, page : "bubble"
            				//			}
            				//			, elements : [{name : "Knowledge in common cloning techniques such as the introduction of genetic material to DNA vectors through TA cloning, blunt and sticky end ligation. Use of common DNA vectors such as pGemT and pBlueScript. Experience in transformation using chemical methods and electroporation."}]}
            				, {name  : "Real time PCR, RT-PCR, PCR"
            					, viewComponent : { app : "bubble"
            						, page : "bubble"
            					}
            					, elements : [{name : "Experience of real time RT-PCR throughout my PhD for the quantification of gene expression in zebrafish hematopoietic cells, in particular from quantifying low expression genes from minute amounts of genetic material (from >5000 FACS sorted cells). Extensive experience with relative quantification (using the delta delta Ct method), the usage of Sybr-Green and Taqman dyes and analysis of gene expression data. Successfully amplified zebrafish tropoelastin transcripts through RT-PCR for sequencing of this novel gene."}]}
            				, {name  : "Digital PCR"
            					, viewComponent : { app : "bubble"
            						, page : "bubble"
            					}
            					, elements : [{name : "Experience in assay design on the RainDrop digital PCR platform."}]}
            				//		, {name  : "Site directed mutagenesis"
            				//			, viewComponent : { app : "bubble"
            				//							, page : "bubble"
            				//			}
            				//			, elements : [{name : "Extensive experience in the use of site directed mutagenesis (with the Stratagene Quikchange site mutagenesis kit) for morpholino rescue experiments."}]}
            				//		, {name  : "*in vitro* transcription"
            				//			, viewComponent : { app : "bubble"
            				//							, page : "bubble"
            				//			}
            				//			, elements : [{name : "Highly proficient in the use of *in vitro* transcription for the construction of RNA transcripts in over-expression experiments and riboprobes for whole mount *in situ* hybridization experiments. This technique was also combined with site directed mutagenesis for morpholino rescue experiments."}]}
            				, {name  : "Northern and Western blotting"
            					, viewComponent : { app : "bubble"
            						, page : "bubble"
            					}
            					, elements : [{name : "Applied northern blotting and hybridization for the detection of alternate splicing in zebrafish tropoelastin using radioactive RNA probes. Western blotting for semi-quantitative analysis of beta-catenin protein amount in zebrafish embryos."}]}
            				]}					
            			]}
                        		, {name : "Information Technology / Bioinformatics Skills"
                        			, viewComponent : { app : "sectionHeader"
                        				, page : "sectionHeader"
                        			}
                        			,elements : [ { name : "Bioinformatics"
                        				, viewComponent : { app : "inlineList"
                        					, page : "inlineList"
                        				}
                        				, elements : [ {name  : "Annotation of variant data"
                        					, viewComponent : { app : "bubble"
                        						, page : "bubble"
                        					}
                        					, elements : [{name : "Annotation of variant data using phenotype databases such as HGMD, OMIM and COSMIC."}]}
                        				, {name  : "Linux Administration and CLI"
                        					, viewComponent : { app : "bubble"
                        						, page : "bubble"
                        					}
                        					, elements : [{name : "Experience in setup of Linux boxes (Ubuntu) for parsing of output data, installation of local instances of Galaxy and common tools such as SIFT/PolyPhen, samtools, etc. Use of Pipes and AWK for data formatting."}]}
                        				, {name  : "Phylogenetic analysis"
                        					, viewComponent : { app : "bubble"
                        						, page : "bubble"
                        					}
                        					, elements : [{name : "Successfully constructed phylogenetic trees for interpretation of data using Phylip and Mega for my MSc and PhD projects."}]}
                        				]}
                        			, {name : "Programming Languages"
                        				, viewComponent : { app : "inlineList"
                        					, page : "inlineList"
                        				}
                        				, elements : [{name  : "HTML/JavaScript: 1 year"
                        					, viewComponent : { app : "bubble"
                        						, page : "bubble"
                        					}
                        					, elements : [{name : "This CV is written in HTML5/JavaScript and served using Node.js"}]}
                        				, {name  : "ActionScript3: 1-2 years"
                        					, viewComponent : { app : "bubble"
                        						, page : "bubble"
                        					}
                        					, elements : [{name : "ActionScript3 is used in the development of the CaptureREAD image acquisition and analysis system from DiagCor."}]}
                        				, {name  : "C++/objective-C: 2 years"
                        					, viewComponent : { app : "bubble"
                        						, page : "bubble"
                        					}
                        					, elements : [{name : "iPhone camera app and structured light scanner software were written in Objective-C and C++ respectively."}]}
                        				, {name  : "C: 1 year"
                        					, viewComponent : { app : "bubble"
                        						, page : "bubble"
                        					}
                        					, elements : [{name : "The adaptive flight controller in the Crazyflie firmware was written in C."}]}
                        				, {name  : "Python: 1 year"
                        					, viewComponent : { app : "bubble"
                        						, page : "bubble"
                        					}
                        					, elements : [{name : "Structured light scanner software was partially written in python."}]}
                        				, {name  : "LUA: <1 year"
                        					, viewComponent : { app : "bubble"
                        						, page : "bubble"
                        					}
                        					, elements : [{name : "Lua is used as the scripting layer of my iPhone camera app. The GUI and post camera effects are scripted in Lua."}]}
                        				, {name  : "OpenGL ES 2.0: <1 year"
                        					, viewComponent : { app : "bubble"
                        						, page : "bubble"
                        					}
                        					, elements : [{name : "GLSL is used for the rendering of the GUI and the real-time post capture effects in my iPhone project. Basic knowledge in the optimization of GLSL programs for PowerVR GPUs."}]}
                        				]}
                        			, { name : "Integrated Development Environments / Version Control"
                        				, viewComponent : { app : "inlineList"
                        					, page : "inlineList"
                        				}
                        				,elements : [{name  : "XCode"
                        					, viewComponent : { app : "bubble"
                        						, page : "bubble"
                        					}
                        					, elements : [{name : "Experience in the use of XCode for iPhone application development."}]}


                        				,{name  : "Eclipse"
                        					, viewComponent : { app : "bubble"
                        						, page : "bubble"
                        					}
                        					, elements : [{name : "Used Eclipse IDE for the development of the adaptive flight controller and structured light 3D scanner."}]}
                        				, {name  : "Adobe Flash Professional"
                        					, viewComponent : { app : "bubble"
                        						, page : "bubble"
                        					}
                        					, elements : [{name : "Adobe Flash Professional is used for the in the development of the CaptureREAD image acquisition and analysis system from DiagCor."}]}
                        				, {name  : "Git"
                        					, viewComponent : { app : "bubble"
                        						, page : "bubble"
                        					}
                        					, elements : [{name : "Used Git for version control for my software projects."}]}
                        				, {name  : "Adobe Photoshop"
                        					, viewComponent : { app : "bubble"
                        						, page : "bubble"
                        					}
                        					, elements : [{name : "Utilization of Adobe Photoshop for images related to my scientific research."}]}
                        				]}
                        			]} */
            , {
                name: "Scientific Publications",
                viewComponent: {
                    app: "list",
                    page: "list",
                    it: "hidden",
                    biotech: "visible"
                }

                ,
                elements: [{
                    name: "**Chung MI, Ma AC, Fung TK, Leung AY.** Characterization of Sry-related HMG box group F genes in zebrafish hematopoiesis. Exp Hematol. 2011 Jul 1. [PubMed](http://www.ncbi.nlm.nih.gov/m/pubmed/21726513)"
                }, {
                    name: "**Chung MI, Miao M, Stahl RJ, Chan E, Parkinson J, Keeley FW.** Sequences and domain structures of mammalian, avian, amphibian and teleost tropoelastins: Clues to the evolutionary history of elastins. Matrix Biol. 2006 Oct;25(8):492-504. [PubMed](http://www.ncbi.nlm.nih.gov/pubmed/16982180)"
                }, {
                    name: "**Ma AC, Chung MI, Liang R, Leung AY.** A DEAB-sensitive aldehyde dehydrogenase regulates hematopoietic stem and progenitor cells development during primitive hematopoiesis in zebrafish embryos. Leukemia. 2010 Dec;24(12):2090-9. [PubMed](http://www.ncbi.nlm.nih.gov/pubmed/20927131)"
                }, {
                    name: "**Ma AC, Chung MI, Liang R, Leung AY.** The role of survivin2 in primitive hematopoiesis during zebrafish development. Leukemia. 2009 Apr;23(4):712-20. [PubMed](http://www.ncbi.nlm.nih.gov/pubmed/19151781)"
                }, {
                    name: "**He D, Chung M, Chan E, Alleyne T, Ha KC, Miao M, Stahl RJ, Keeley FW, Parkinson J.** Comparative genomics of elastin: Sequence analysis of a highly repetitive protein. Matrix Biol. 2007 Sep;26(7):524-40. [PubMed](http://www.ncbi.nlm.nih.gov/pubmed/17628459)"
                }]
            }


        ]
    }]
}];
